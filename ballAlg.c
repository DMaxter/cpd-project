#include <stdio.h>
#include <float.h>
#include <math.h>
#include <omp.h>

#include <algorithm>

#include "ballAlgUtils.c"
#include "gen_points.c"

void print_point(double *point) {
  int i;
  for (i = 0; i < DIM-1; i++)
    printf("%.6f ", point[i]);
  printf("%.6f\n", point[DIM-1]);
}

typedef struct node {
  double radius;
  double *center;
  struct node* L;
  struct node* R;
} node_t;

int DIM;
double **pts;
long n_points;
double *centers;

double *ab, *base, *aux1;

node_t *nodes;
long free_node;

/**
 * Get the the point that is furthest from origin
 *
 * @param group - pointer to current context
 * @param size - number of points to analyze
 * @param origin - index of the point
 *
 * @returns the index of the furthest point from origin
 */
long furthest_from(point_ref* group, long size, double *origin) {
  long res_idx = 0;
  double dist, max_dist = -1;

  for (long i = 0; i < size; ++i) {
    dist = dist_sq(origin, pts[group[i].idx]);
    if (dist > max_dist) {
      res_idx = group[i].idx;
      max_dist = dist;
    }
  }

  return res_idx;
}


/**
 * Compare function for std::nth_element
 *
 * @param a - first element
 * @param b - second element
 *
 * @returns compare value
 */
bool cmp(const point_ref& a, const point_ref& b) {
  return a.prj_x < b.prj_x;
}

/**
 * Get the center of the ball
 *
 * @param group - the list of points
 * @param size - the number of points
 * @param a - the point a (one of the furthest)
 * @param center - where the result will be stored
 */
void compute_center(point_ref* group, long size, double* a, double* center) {
  long mid = size / 2;

  std::nth_element(group, &group[mid], &group[size], cmp);

  proj_hack(group[mid].prj_x, base, a, center);
  
  // if even compute second median
  if (size % 2 == 0) {
    proj_hack(std::max_element(group, group + mid, cmp)->prj_x, base, a, aux1);
    midpoint(center, aux1, center);
  }
}

/**
 * Computes ball tree nodes recursively
 *
 * @param group - the list of points assigned to the node
 * @param size - the number of points
 *
 * @returns computed node
 */
node_t* ballAlg(point_ref* group, long size) {
  node_t *node = &nodes[free_node++];

  if (size == 1) { // 1 point
    node->center = pts[group[0].idx];
    return node;
  }

  node->center = &centers[free_node*DIM];
  long a, b;
  if (size == 2) { // 2 points
    a = group[0].idx;
    b = group[1].idx;

    midpoint(pts[a], pts[b], node->center);

  } else { // >2 points

    a = furthest_from(group, size, pts[group[0].idx]);
    b = furthest_from(group, size, pts[a]);

    sub(pts[b], pts[a], ab);
    mul(ab, 1.0 / dot(ab, ab), base);

    // compute first dimension of projection
    for (long i = 0; i < size; i++)
      group[i].prj_x = proj_x(ab, base[0], pts[a], pts[group[i].idx]);

    compute_center(group, size, pts[a], node->center);

    // compute radius point
    a = furthest_from(group, size, node->center);
  }

  node->radius = sqrt(dist_sq(node->center, pts[a]));
  node->L = ballAlg(group, size/2);
  node->R = ballAlg(&group[size/2], size/2 + size%2);

  return node;
}


/**
 * Allocate array of all nodes needed for the tree
 * Stored in global variable 'nodes'
 */
void allocate_nodes() {
  long size = 2 * n_points - 1;

  nodes = (node_t*)malloc(size * sizeof(node_t));

  centers = (double*)malloc(size * DIM * sizeof(double));

  if (!nodes || !centers) {
    printf("Error allocating array of nodes, exiting.\n");
    exit(4);
  }

  for (long i = 0; i < size; i++) {
    nodes[i].radius = 0.0;
    nodes[i].L = nodes[i].R = NULL;
  }

  free_node = 0;
}


/**
 * Build the ball tree
 * Initializes the structures and calls the recursive node build
 *
 * @returns tree root
 */
node_t* build_tree() {
  point_ref *refs = (point_ref*)malloc(n_points * sizeof(point_ref));
  if (!refs) {
    printf("Error allocating point references.\n");
    exit(4);
  }
  for (long i = 0; i < n_points; ++i) {
    refs[i].idx = i;
  }

  allocate_nodes();

  ab = (double*)malloc(3 * DIM * sizeof(double));
  if (!ab) {
    printf("Error allocating auxiliary vectors.\n");
    exit(4);
  }
  base = &ab[DIM];
  aux1 = &ab[2*DIM];

  return ballAlg(refs, n_points);
}


/**
 * Print each node recursively
 *
 * @param node - node to print
 * @param id - id associated with the node
 *
 * @returns max index of current branch
 */
/*long dump_node(node_t* node, long id) {
  if (!node->L) {
    printf("%ld %d %d %.6f ", id, -1, -1, node->radius);
    print_point(node->center);
    return id;
  }

  long maxL = dump_node(node->L, id + 1);
  long maxR = dump_node(node->R, maxL + 1);

  printf("%ld %ld %ld %.6f ", id, id+1, maxL+1, node->radius);
  print_point(node->center);

  return maxL > maxR ? maxL : maxR;
}*/
void dump_node(node_t* node, long id) {
  if (!node->L) {
    printf("%ld %d %d %.6f ", id, -1, -1, node->radius);
    print_point(node->center);
    return;
  }

  dump_node(node->L, 2*id + 1);
  dump_node(node->R, 2*id + 2);

  printf("%ld %ld %ld %.6f ", id, 2*id + 1, 2*id + 2, node->radius);
  print_point(node->center);
}

/**
 * Print the tree to stdout
 *
 * @param root - root of the tree
 */
void dump_tree(node_t* root) {
  printf("%d %ld\n", DIM, 2*n_points-1);
  dump_node(root, 0);
}


int main(int argc, char *argv[]) {
  double exec_time;
  node_t* root;

  exec_time = -omp_get_wtime();
  pts = get_points(argc, argv, &n_points);

  root = build_tree();

  exec_time += omp_get_wtime();
  fprintf(stderr, "%.1f\n", exec_time);

  dump_tree(root);

  return 0;
}
