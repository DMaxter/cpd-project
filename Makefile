SOURCES=$(wildcard *.c)
CFLAGS=-O3 -Wall -Wextra -fopenmp
LDFLAGS=-lm
CC=g++

# Set debug flags if needed
ifdef DEBUG
	CFLAGS+= -ggdb3 -pg
endif

all: ballAlg ballAlg-omp ballAlg-mpi

ballAlg: ballAlg.o ballAlgUtils.o gen_points.o
	@${CC} ${CFLAGS} -o $@ $< ${LDFLAGS}

ballAlg-omp: ballAlg-omp.o ballAlgUtils.o gen_points.o
	@${CC} ${CFLAGS} -o $@ $< ${LDFLAGS}

ballAlg-mpi: ballAlg-mpi.c
	@mpic++ ${CFLAGS} -o $@ $< ${LDFLAGS}

%.o: %.c
	@${CC} $(CFLAGS) -o $@ -c $<

clean:
	@\rm -rf *.o ballAlg ballAlg-omp ballAlg-mpi
