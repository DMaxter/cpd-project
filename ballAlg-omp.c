#include <stdio.h>
#include <float.h>
#include <math.h>
#include <omp.h>

#include <algorithm>

#include "ballAlgUtils.c"
#include "gen_points.c"

void print_point(double *point) {
  int i;
  for (i = 0; i < DIM-1; i++)
    printf("%.6f ", point[i]);
  printf("%.6f\n", point[DIM-1]);
}


typedef struct {
  long idx;
  double dist;
} dist_pair;

typedef struct node {
  double radius;
  double *center;
  struct node* L;
  struct node* R;
} node_t;


int DIM;
double **pts;
long n_points;
double *centers;

double **ab, **base, **aux;

node_t *nodes;

/**
 * Get the the point that is furthest from origin
 *
 * @param group - pointer to current context
 * @param size - number of points to analyze
 * @param origin - index of the point
 *
 * @returns the index of the furthest point from origin
 */
long furthest_from(point_ref* group, long size, double *origin) {
  long res_idx = 0;
  double dist, max_dist = -1;

  for (long i = 0; i < size; ++i) {
    dist = dist_sq(origin, pts[group[i].idx]);
    if (dist > max_dist) {
      res_idx = group[i].idx;
      max_dist = dist;
    }
  }

  return res_idx;
}


/**
 * Compare function for std::nth_element
 *
 * @param a - first element
 * @param b - second element
 *
 * @returns compare value
 */
bool cmp(const point_ref& a, const point_ref& b) {
  return a.prj_x < b.prj_x;
}

/**
 * Get the center of the ball
 *
 * @param group - the list of points
 * @param size - the number of points
 * @param a - the point a (one of the furthest)
 * @param center - where the result will be stored
 */
void compute_center(point_ref* group, long size, double* a, double* center) {
  int id = omp_get_thread_num();
  long mid = size / 2;

  std::nth_element(group, &group[mid], &group[size], cmp);

  proj_hack(group[mid].prj_x, base[id], a, center);

  // if even compute second median
  if (size % 2 == 0) {
    proj_hack(std::max_element(group, group + mid, cmp)->prj_x, base[id], a, aux[id]);
    midpoint(center, aux[id], center);
  }
}

/**
 * Computes ball tree nodes recursively, uses omp task construct
 *
 * @param group - the list of points assigned to the node
 * @param size - the number of points
 * @param nodes_group - portion of the global array of nodes assigned to the node
 * @param link - pointer to the branch of the parent that this node must be linked to
 */
void ballAlg(point_ref* group, long size, node_t* nodes_group, node_t **link) {
  int id = omp_get_thread_num();

  // get node from array of nodes
  long node_idx = size-1-(size>1 && size%2);
  node_t *node = &nodes_group[node_idx];

  // assign node to parent's L or R
  if (link) *link = node;
  
  if (size == 1) { // 1 point
    node->center = pts[group[0].idx];
    return;
  }

  long a, b;
  if (size == 2) { // 2 points
    a = group[0].idx;
    b = group[1].idx;

  	midpoint(pts[a], pts[b], node->center);

  } else { // >2 points

    a = furthest_from(group, size, pts[group[0].idx]);
	  b = furthest_from(group, size, pts[a]);

	  sub(pts[b], pts[a], ab[id]);
	  mul(ab[id], 1.0 / dot(ab[id], ab[id]), base[id]);

	  // compute projections
	  for (long i = 0; i < size; i++)
	    group[i].prj_x = proj_x(ab[id], base[id][0], pts[a], pts[group[i].idx]);

	  compute_center(group, size, pts[a], node->center);

	  // compute radius point
	  a = furthest_from(group, size, node->center);
  }

  node->radius = sqrt(dist_sq(node->center, pts[a]));

  #pragma omp task
  ballAlg(group, size/2, nodes_group, &node->L);

  #pragma omp task
  ballAlg(&group[size/2], size/2 + size%2, &nodes_group[node_idx+1], &node->R);
}


/**
 * Allocate array of all nodes needed for the tree
 * Stored in global variable 'nodes'
 */
void allocate_nodes() {
  long size = 2 * n_points - 1;

  nodes = (node_t*)malloc(size * sizeof(node_t));

  centers = (double*)malloc(size * DIM * sizeof(double));

  if (!nodes || !centers) {
    printf("Error allocating array of nodes, exiting.\n");
    exit(4);
  }

}

/**
 * Build the ball tree
 * Initializes the structures and calls the recursive node build
 *
 * @returns tree root
 */
node_t* build_tree() {
  node_t* root = NULL;
  long num_nodes = 2 * n_points - 1;

  point_ref *refs = (point_ref*)malloc(n_points * sizeof(point_ref));
  if (!refs) {
    printf("Error allocating point references.\n");
    exit(4);
  }

  allocate_nodes();

  // variable for parallel toot comnputation
  long size = n_points;
  long node_idx = size-1-(size>1 && size%2);
  root = &nodes[node_idx];
  node_t* node = root;
  point_ref* group = refs;
  node_t* nodes_group = nodes;

  long a, b;
  int done = 0;

  double dist;
  dist_pair maxPair_a={0,-1}, maxPair_b={0,-1}, maxPair_r={0,-1};

  #pragma omp declare reduction(max_dist_idx : dist_pair : omp_out = omp_in.dist > omp_out.dist ? omp_in : omp_out) initializer(omp_priv = {0, -1})

  #pragma omp parallel
  {
    int n = omp_get_num_threads();
    int id = omp_get_thread_num();

    /////////////////////////
    // initialize structures

    #pragma omp single
    {
      ab = (double**)malloc(3 * n * sizeof(double*));
      if (!ab) {
        printf("Error allocating auxiliary vectors.\n");
        exit(4);
      }
      base = &ab[n];
      aux = &ab[2*n];
    }


    ab[id] = (double*)malloc(3 * DIM * sizeof(double));
    if (!ab[id]) {
      printf("Error allocating auxiliary vectors.\n");
      exit(4);
    }
    base[id] = &ab[id][DIM];
    aux[id] = &ab[id][2*DIM];

    #pragma omp for
    for (long i = 0; i < n_points; ++i) {
      refs[i].idx = i;
    }

    #pragma omp for
    for (long i = 0; i < num_nodes; i++) {
      nodes[i].radius = 0.0;
      nodes[i].L = nodes[i].R = NULL;
      nodes[i].center = &centers[i*DIM];
    }

    /////////////////////////

    // if <10 points go directly to tasks
    #pragma omp single
    if (size < 10) {
      done = 1;
      #pragma omp task
      ballAlg(refs, n_points, nodes, NULL);
    }
    
    // else do root node with all threads
    if (!done) {

    	// compute point a
      #pragma omp for private(dist) reduction(max_dist_idx : maxPair_a)
      for (long i = 0; i < size; ++i) {
        dist = dist_sq(pts[group[0].idx], pts[group[i].idx]);
        if (dist > maxPair_a.dist) {
          maxPair_a.idx = group[i].idx;
          maxPair_a.dist = dist;
        }
      }

      // compute point b
      #pragma omp for private(dist) reduction(max_dist_idx : maxPair_b)
      for (long i = 0; i < size; ++i) {
        dist = dist_sq(pts[maxPair_a.idx], pts[group[i].idx]);
        if (dist > maxPair_b.dist) {
          maxPair_b.idx = group[i].idx;
          maxPair_b.dist = dist;
        }
      }

      // set aux variables
      #pragma omp single
      {
        a = maxPair_a.idx;
        b = maxPair_b.idx;
        sub(pts[b], pts[a], ab[0]);
        mul(ab[0], 1.0 / dot(ab[0], ab[0]), base[0]);
      }

      // compute projections
      #pragma omp for
      for (long i = 0; i < size; i++)
        group[i].prj_x = proj_x(ab[0], base[0][0], pts[a], pts[group[i].idx]);

      // master thread computer center
      #pragma omp master
      compute_center(group, size, pts[a], node->center);
      #pragma omp barrier

      // compute radius point
      #pragma omp for private(dist) reduction(max_dist_idx : maxPair_r)
      for (long i = 0; i < size; ++i) {
        dist = dist_sq(node->center, pts[group[i].idx]);
        if (dist > maxPair_r.dist) {
          maxPair_r.idx = group[i].idx;
          maxPair_r.dist = dist;
        }
      }

      // continue recursion with tasks
      #pragma omp single nowait
      {
        node->radius = sqrt(maxPair_r.dist);

        #pragma omp task
        ballAlg(group, size/2, nodes_group, &node->L);
        
        #pragma omp task
        ballAlg(&group[size/2], size/2 + size%2, &nodes_group[node_idx+1], &node->R);
      }
    }
  }

  return root;
}


/**
 * Print each node recursively
 *
 * @param node - node to print
 * @param id - id associated with the node
 *
 * @returns max index of current branch
 */
long dump_node(node_t* node, long id) {
  if (!node->L) {
    printf("%ld %d %d %.6f ", id, -1, -1, node->radius);
    print_point(node->center);
    return id;
  }

  long maxL = dump_node(node->L, id + 1);
  long maxR = dump_node(node->R, maxL + 1);

  printf("%ld %ld %ld %.6f ", id, id+1, maxL+1, node->radius);
  print_point(node->center);

  return maxL > maxR ? maxL : maxR;
}

/**
 * Print the tree to stdout
 *
 * @param root - root of the tree
 */
void dump_tree(node_t* root) {
  printf("%d %ld\n", DIM, 2*n_points-1);
  dump_node(root, 0);
}



int main(int argc, char *argv[]) {
  double exec_time;
  node_t* root;

  exec_time = -omp_get_wtime();
  pts = get_points(argc, argv, &n_points);

  root = build_tree();

  exec_time += omp_get_wtime();
  fprintf(stderr, "%.1f\n", exec_time);

  dump_tree(root);

  return 0;
}
