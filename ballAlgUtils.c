#include <stdlib.h>
#include <stdint.h>

extern int DIM;

typedef struct {
  long idx;
  double prj_x;
} point_ref;


/**
 * Calculate the dot product between two vectors
 *
 * @param p1 - one vector
 * @param p2 - the other vector
 *
 * @returns the value of the dot product `p1 . p2`
 */
double dot(double *p1, double *p2) {
  double dot = 0;

  for (int i = 0; i < DIM; i++) {
    dot += p1[i] * p2[i];
  }

  return dot;
}

/**
 * Subtract two vectors (or points)
 *
 * @param p1 - one vector (or point)
 * @param p2 - the other vector (or point)
 * @param res - where the result `p2 - p1` will be stored
 */
void sub(double *p1, double *p2, double *res) {
  for (int i = 0; i < DIM; i++) {
    res[i] = p2[i] - p1[i];
  }
}

/**
 * Add a vector to a point (or other vector)
 *
 * @param v - the vector
 * @param p - the point (or other vector)
 * @param res - where the result `p + v` will be stored
 */
void add(double *v, double *p, double *res) {
  for (int i = 0; i < DIM; i++) {
    res[i] = v[i] + p[i];
  }
}

/**
 * Multiply a scalar to a vector
 *
 * @param v - the vector
 * @param s - the scalar
 * @param res - where the result of `s * v` will be stored
 */
void mul(double *v, double s, double *res) {
  for (int i = 0; i < DIM; i++) {
    res[i] = v[i] * s;
  }
}

/**
 * Applies v1 * s + v2
 *
 * @param v1 - first vector
 * @param s - the scalar
 * @param v2 - second vector
 * @param res - where the result will be stored
 */
void mul_add(double *v1, double s, double *v2, double *res) {
  for (int i = 0; i < DIM; i++) {
    res[i] = v1[i] * s + v2[i];
  }
}

/**
 * Compute the middle point of two vectors (or points)
 *
 * @param p1 - one vector (or point)
 * @param p2 - the other vector (or point)
 * @param res - where the midpoint will be stored
 */
void midpoint(double *p1, double *p2, double *res) {
  for (int i = 0; i < DIM; i++) {
    res[i] = (p1[i] + p2[i]) / 2.0;
  }
}

/**
 * Calculate the squared distance between two points
 *
 * @param p1 - one point
 * @param p2 - the other point
 *
 * @returns the squared distance between p1 and p2
 */
double dist_sq(double *p1, double *p2) {
  double d = 0, diff;

  for (int i = 0; i < DIM; i++) {
    diff = p2[i] - p1[i];
    d += diff * diff;
  }

  return d;
}

/**
 * Calculate the projection of p, on all coordinates, over ab
 *
 * @param ab - vector ab
 * @param base - pre-computed vector representing (`ab / ||ab||²`)
 * @param a - point a
 * @param p - point p
 * @param res - where the result of the projection will be stored
 */
void proj(double *ab, double *base, double *a, double *p, double *res) {
  double num = 0;

  for (int i = 0; i < DIM; i++) {
    num += (p[i] - a[i]) * ab[i];
  }

  mul_add(base, num, a, res);
}

/**
 * Optimized projection computation for a point p, over ab
 *
 * @param precalc - precomputed prj_x of point p
 * @param base - pre-computed vector representing (`ab / ||ab||²`)
 * @param a - point a
 * @param res - where the result of the projection will be stored
 */
void proj_hack(double precalc, double *base, double *a, double *res) {
  double num = (precalc - a[0]) / base[0];

  mul_add(base, num, a, res);
}

/**
 * Calculate the projection of p, on the first coordinate, over ab
 *
 * @param ab - vector ab
 * @param base_x -  first coordinate of the pre-computed vector representing (`ab / ||ab||²`)
 * @param a - point a
 * @param p - point p
 *
 * @returns the first coordinate of p projected over ab
 */
double proj_x(double *ab, double base_x, double *a, double *p) {
  double num = 0;

  for (int i = 0; i < DIM; i++) {
    num += (p[i] - a[i]) * ab[i];
  }

  return a[0] + (base_x * num);
}