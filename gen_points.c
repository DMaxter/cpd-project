#include <stdio.h>
#include <stdlib.h>

#define RANGE 10

extern int DIM;

extern void print_point(double *);

double **create_array_pts(long np) {
  double *_p_arr;
  double **p_arr;

  // Allocate a matrix of DIM x NPOINTS
  _p_arr = (double *)malloc(DIM * np * sizeof(double));

  // Allocate an array of NPOINTS (that will point to addresses of _p_arr)
  p_arr = (double **)malloc(np * sizeof(double *));

  if ((_p_arr == NULL) || (p_arr == NULL)) {
    printf("Error allocating array of points, exiting.\n");
    exit(4);
  }

  // Set values of array pointer
  for (long i = 0; i < np; i++)
    p_arr[i] = &_p_arr[i * DIM];

  return p_arr;
}

double **get_points(int argc, char *argv[], long *np) {
  double **pt_arr;
  unsigned seed;
  long i;
  int j;

  if (argc != 4) {
    printf("Usage: %s <n_dims> <n_points> <seed>\n", argv[0]);
    exit(1);
  }

  DIM = atoi(argv[1]);
  if (DIM < 2) {
    printf("Illegal number of dimensions (%d), must be above 1.\n", DIM);
    exit(2);
  }

  *np = atol(argv[2]);
  if (*np < 1) {
    printf("Illegal number of points (%ld), must be above 0.\n", *np);
    exit(3);
  }

  seed = atoi(argv[3]);
  srandom(seed);

  pt_arr = (double **)create_array_pts(*np);

  for (i = 0; i < *np; i++)
    for (j = 0; j < DIM; j++)
      pt_arr[i][j] = RANGE * ((double)random()) / RAND_MAX;

#ifdef DEBUG
  for (i = 0; i < *np; i++)
    print_point(pt_arr[i]);
#endif

  return pt_arr;
}
