#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <omp.h>
#include <mpi.h>

#include <algorithm>

#include "ballAlgUtils.c"
#include "gen_points.c"

typedef struct node {
  double radius;
  double *center;
  struct node* L;
  struct node* R;
} node_t;

// MPI
int ME;
int NODES;

// BallAlg
int DIM;
double **pts;
double **ab, **base, **aux;
long n_points;
double *centers;

node_t *solo_nodes = NULL;
long num_solo = 0;
long free_node_solo = 0;

long redundant_start = -1;
long redundant_start_size = -1;
int num_redundant = 0;
node_t *redundant_nodes = NULL;
int free_node_redundant = 0;

node_t *dummy = NULL;


void print_point(double *point) {
  int i;
  for (i = 0; i < DIM-1; i++)
    printf("%.6f ", point[i]);
  printf("%.6f\n", point[DIM-1]);
}



long furthest_from(point_ref* group, long size, double *origin) {
  long res_idx = 0;
  double dist, max_dist = -1;

  for (long i = 0; i < size; ++i) {
    dist = dist_sq(origin, pts[group[i].idx]);
    if (dist > max_dist) {
      res_idx = group[i].idx;
      max_dist = dist;
    }
  }

  return res_idx;
}

bool cmp(const point_ref& a, const point_ref& b) {
  return a.prj_x < b.prj_x;
}

void compute_center(point_ref* group, long size, double* a, double* center, int id) {
  long mid = size / 2;

  std::nth_element(group, &group[mid], &group[size], cmp);

  proj_hack(group[mid].prj_x, base[id], a, center);

  // if even compute second median
  if (size % 2 == 0) {
    proj_hack(std::max_element(group, group + mid, cmp)->prj_x, base[id], a, aux[id]);
    midpoint(center, aux[id], center);
  }
}

void ballAlgAll(point_ref *group, long size, long node_idx, node_t **link) {
  int id = omp_get_thread_num();

  // get node from array of nodes
  node_t *node = &solo_nodes[node_idx];

  // assign node to parent's L or R
  if (link) *link = node;

  if (size == 1) { // 1 point
    node->center = pts[group[0].idx];
    return;
  }

  long a, b;
  if (size == 2) { // 2 points
    a = group[0].idx;
    b = group[1].idx;

    midpoint(pts[a], pts[b], node->center);
  } else {
    a = furthest_from(group, size, pts[group[0].idx]);
    b = furthest_from(group, size, pts[a]);

    sub(pts[b], pts[a], ab[id]);
    mul(ab[id], 1.0 / dot(ab[id], ab[id]), base[id]);
    //sub(pts[b], pts[a], ab);
    //mul(ab, 1.0 / dot(ab, ab), base);

    // compute first dimension of projection
    for (long i = 0; i < size; i++)
      //group[i].prj_x = proj_x(ab, base[0], pts[a], pts[group[i].idx]);
      group[i].prj_x = proj_x(ab[id], base[id][0], pts[a], pts[group[i].idx]);

    compute_center(group, size, pts[a], node->center, id);

    // compute radius point
    a = furthest_from(group, size, node->center);
  }

  node->radius = sqrt(dist_sq(node->center, pts[a]));

  #pragma omp task
  ballAlgAll(group, size/2, node_idx+1, &node->L);

  #pragma omp task
  ballAlgAll(&group[size/2], size/2 + size%2, node_idx+2*(size/2), &node->R);
}

void doSoloRegion(point_ref *group, long size, long node_idx, node_t **link) {
  if (redundant_start_size == -1)
      redundant_start_size = size;

  #pragma omp parallel
  {
    int n = omp_get_num_threads();
    int id = omp_get_thread_num();

    #pragma omp single
    {
      free(ab[0]);
      free(ab);
      ab = (double**)malloc(3 * n * sizeof(double*));
      if (!ab) {
        printf("Error allocating auxiliary vectors.\n");
        exit(4);
      }
      base = &ab[n];
      aux = &ab[2*n];
    }


    ab[id] = (double*)malloc(3 * DIM * sizeof(double));
    if (!ab[id]) {
      printf("Error allocating auxiliary vectors.\n");
      exit(4);
    }
    base[id] = &ab[id][DIM];
    aux[id] = &ab[id][2*DIM];

    #pragma omp barrier
    #pragma omp single nowait
    {
      #pragma omp task
      ballAlgAll(group, size, node_idx, link);
    }
  }

}

node_t* ballAlg(point_ref* group, long size, int mpiNodes, int mpiNodes_start) {
  long a, b;
  node_t *node;

  if (ME == mpiNodes_start) {
    node = &redundant_nodes[free_node_redundant++];

    if (redundant_start_size == -1)
      redundant_start_size = size;
  } else {
    node = dummy;
  }

  if (size == 1) { // 1 point
    node->center = pts[group[0].idx];
    return node;
  }

  if (size == 2) { // 2 points
    a = group[0].idx;
    b = group[1].idx;

    midpoint(pts[a], pts[b], node->center);
  } else {
    a = furthest_from(group, size, pts[group[0].idx]);
    b = furthest_from(group, size, pts[a]);

    sub(pts[b], pts[a], ab[0]);
    mul(ab[0], 1.0 / dot(ab[0], ab[0]), base[0]);

    // compute first dimension of projection
    for (long i = 0; i < size; i++)
      group[i].prj_x = proj_x(ab[0], base[0][0], pts[a], pts[group[i].idx]);

    compute_center(group, size, pts[a], node->center, 0);

    // compute radius point
    a = furthest_from(group, size, node->center);
  }

  node->radius = sqrt(dist_sq(node->center, pts[a]));


  int mpiNodesL = mpiNodes/2;
  if (ME >= mpiNodes_start + mpiNodesL) { // go R
    int mpiNodesR = mpiNodesL + mpiNodes%2;
    if (mpiNodesR == 1) {
      // go solo
      doSoloRegion(&group[size/2], size/2 + size%2, 0, &node->R);
    } else {
      // continues redundant
      node->R = ballAlg(&group[size/2], size/2 + size%2, mpiNodesR, mpiNodes_start + mpiNodesL);
    }

  } else { // go L
    if (mpiNodesL == 1) {
      // go solo
      doSoloRegion(group, size/2, 0, &node->L);
    } else {
      // continues redundant
      node->L = ballAlg(group, size/2, mpiNodesL, mpiNodes_start);
    }
  }

  return node;
}

void allocate_nodes() {
  long total = num_solo + num_redundant + 1;
  node_t *nodes = (node_t*)malloc(total * sizeof(node_t));

  centers = (double*)malloc(total * DIM * sizeof(double));

  if (!nodes || !centers) {
    printf("Error allocating array of nodes, exiting.\n");
    exit(4);
  }

  for (long i = 0; i < total; i++) {
    nodes[i].radius = 0.0;
    nodes[i].L = nodes[i].R = NULL;
    nodes[i].center = &centers[i*DIM];
  }

  redundant_nodes = nodes;
  solo_nodes = &nodes[num_redundant];
  dummy = &nodes[total-1];
}


void calc_needed_tree_nodes() {
  int n = NODES;
  int mpi_start_id = 0;
  long num_points = n_points;
  int red = 0;
  long binary_id = 0;
  long binary_id_start = ME == 0 ? 0 : -1;

  while (n > 1 && num_points > 0) {
    if (mpi_start_id == ME) red += 1;

    if (ME >= mpi_start_id + n/2) { // go R
      mpi_start_id = mpi_start_id + n/2;
      n = n/2 + n%2;
      binary_id = binary_id + 2 * (num_points/2);
      num_points = num_points/2 + (num_points>1 && num_points%2);

      if (mpi_start_id == ME && num_points > 0 && binary_id_start == -1) {
        binary_id_start = binary_id;
      }

    } else { // go L
      n /= 2;
      num_points /= 2;
      binary_id = binary_id + 1;
    }
  }

  num_redundant = red;
  num_solo = 2 * num_points - 1 + (num_points == 0);
  redundant_start = binary_id_start;
}

node_t* build_tree() {
  calc_needed_tree_nodes();
  //printf("total: %d, id: %d, points: %ld, red: %d, solo: %ld, start: %ld\n", NODES, ME, n_points, num_redundant, num_solo, redundant_start);

  allocate_nodes();

  point_ref *refs = (point_ref*) malloc(n_points * sizeof(point_ref));
  if (!refs) {
    printf("Error allocating point references.\n");
    exit(4);
  }

  for (long i = 0; i < n_points; ++i) {
    refs[i].idx = i;
  }


  int n = 1;
  int id = 0;
  ab = (double**)malloc(3 * n * sizeof(double*));
  if (!ab) {
    printf("Error allocating auxiliary vectors.\n");
    exit(4);
  }
  base = &ab[n];
  aux = &ab[2*n];

  ab[id] = (double*)malloc(3 * DIM * sizeof(double));
  if (!ab[id]) {
    printf("Error allocating auxiliary vectors.\n");
    exit(4);
  }
  base[id] = &ab[id][DIM];
  aux[id] = &ab[id][2*DIM];



  if (NODES == 1) {
    doSoloRegion(refs, n_points, 0, NULL);
    return &solo_nodes[0];
  }
  return ballAlg(refs, n_points, NODES, 0);
}

void dump_node(node_t* node, long id, long size) {
  if (!node->L) {
    printf("%ld %d %d %.6f ", id, -1, -1, node->radius);
    print_point(node->center);
    return;
  }

  dump_node(node->L, id + 1, size/2);
  dump_node(node->R, id + 2*(size/2), size/2+size%2);

  printf("%ld %ld %ld %.6f ", id, id + 1, id + 2*(size/2), node->radius);
  print_point(node->center);
}


void dump_tree() {
  if (ME == 0) printf("%d %ld\n", DIM, 2*n_points-1);

  long size = redundant_start_size;

  long id = redundant_start;
  for (int i = 0; i < num_redundant; ++i) {
    node_t *node = &redundant_nodes[i];
    if (!node->L)
      printf("%ld %d %d %.6f ", id, -1, -1, node->radius);
    else
      printf("%ld %ld %ld %.6f ", id, id + 1, id + 2*(size/2), node->radius);
    print_point(node->center);
    id = id + 1;
    size = size/2;
  }

  if (num_solo > 0) dump_node(&solo_nodes[0], id, size);
  fflush(stdout);
}

int main(int argc, char *argv[]) {
  double exec_time, global_exec_time;

  exec_time = -MPI_Wtime();
  MPI_Init(&argc, &argv);
  MPI_Comm_size(MPI_COMM_WORLD, &NODES);
  MPI_Comm_rank(MPI_COMM_WORLD, &ME);

  pts = get_points(argc, argv, &n_points);

  build_tree();

  MPI_Barrier(MPI_COMM_WORLD);
  exec_time += MPI_Wtime();

  MPI_Reduce(&exec_time, &global_exec_time, 1, MPI_DOUBLE, MPI_MAX, 0, MPI_COMM_WORLD);

  if (ME == 0) {
    fprintf(stderr, "%.1f\n", global_exec_time);
    fflush(stderr);
  }

  MPI_Barrier(MPI_COMM_WORLD);
  for(int i = 0; i < NODES; i++) {
    MPI_Barrier(MPI_COMM_WORLD);
    if (i == ME) dump_tree();
    MPI_Barrier(MPI_COMM_WORLD);
  }

  MPI_Finalize();

  return 0;
}
